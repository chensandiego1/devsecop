#use the prebuild go image from redhat 
FROM registry.access.redhat.com/ubi8/go-toolset as builder

#add the application zip file to the container
ADD tegola-0.14.x.zip . 

#unzip the file since docker cannot uncompress zip file
RUN unzip tegola-0.14.x.zip

#go to the application and try to compile it
WORKDIR  /opt/app-root/src/tegola-0.14.x/cmd/tegola

RUN go build -mod vendor
#make the file executable 
RUN chmod a+x tegola
#ENTRYPOINT [ "./tegola" ] 
 


# Create minimal deployment image, just alpine & the binary
FROM registry.access.redhat.com/ubi8-init


#remove unneeded nodejs
#RUN dnf remove nodejs
RUN useradd -u 1234 appuser
USER appuser

#move the executable file to the deployment image
COPY --from=builder /opt/app-root/src/tegola-0.14.x/cmd/tegola .
 
ENTRYPOINT [ "./tegola" ] 